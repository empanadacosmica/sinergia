'use strict';
/**
 * Module dependencies.
 */
var init = require('./config/init')(),
    config = require('./config/config'),
    mongoose = require('mongoose'),
    http = require('http');
/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Bootstrap db connection
var db = mongoose.connect(config.db);

// Init the express application
var app = require('./config/express')(db);

// Bootstrap passport config
require('./config/passport')();

// Start the app by listening on <port>
var server = http.createServer(app);
server.listen(config.port);
setTimeout(function(){
    require('./config/realtime')(app.io, app , server);
},2000);


exports = module.exports = app;

// Logging initialization
console.log('Sinergia application started on port ' + config.port);