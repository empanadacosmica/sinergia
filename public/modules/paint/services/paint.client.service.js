'use strict';

//Menu service used for managing  menus
angular.module('paint').factory('Upload', function($resource) {
    var resource = $resource('/upload');
    return resource;
});