'use strict';

//Menu service used for managing  menus
angular.module('paint').factory('Colaborative',['$resource','socketFactory',
    function($resource, socketFactory) {
        var colaborative = {};
        colaborative.realtime=socketFactory();
        return colaborative;
    }
]);