'use strict';

// Setting up route
angular.module('paint').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider) {
        // Redirect to home view when route not found

        // Home state routing
        $stateProvider.
            state('paint', {
                url: '/paint',
                templateUrl: 'modules/paint/views/paint.client.view.html',
                controller: 'paintController'
            });
    }
]);