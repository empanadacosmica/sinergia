'use strict';


angular.module('paint').controller('paintController', ['$rootScope','$scope', 'Authentication','Oeuvres','$timeout','Colaborative',
    function($rootScope, $scope, Authentication, Oeuvres, $timeout, Colaborative) {

        var canvas = document.getElementById('paint-canvas');
        var TIME_MSG=0;
        var points = [];

        Colaborative.realtime.on('mymessage', function ( data) {
            if(points.length>1){
                var tmpPoint =points[points.length-1];
                points[points.length-1] =data.position;
                points[points.length-2]= tmpPoint;
                console.log(points);
                var distance = $scope.brush.interpol.getDistance(points[0], points[1]);
                var step = distance  * 0.1;
                var angle = Math.atan2(points[0].x - points[1].x, points[0].y - points[1].y);

                var pos = {};
                for (var r = 0; r < 10; r++) {
                    pos.x = points[1].x + ( Math.sin(angle)) * (distance) * (r / step);
                    pos.y = points[1].y + ( Math.cos(angle)) * (distance) * (r / step);
                    drawing.graphics.beginFill('#ff0000');
                    drawing.graphics.drawCircle(pos.x  , pos.y , 10) ;
                }
                wrapper.updateCache('source-over');

            }else{
                points.push(data);
            }


        });

        $scope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if(fromState.name === 'paint'){

                    var oeuvre = new Oeuvres({
                        title: 'obra 1',
                        img: canvas.toDataURL()
                    });
                    $rootScope.uploading.is=true;
                    $rootScope.uploading.status=null;

                    oeuvre.$save(function(response) {
                        $rootScope.uploading.is=false;
                        $rootScope.uploading.ocultar=false;
                        $timeout(function(){
                            $rootScope.uploading.status='success';
                        },TIME_MSG);

                    }, function(errorResponse) {
                        $rootScope.uploading.is=false;
                        $rootScope.uploading.ocultar=false;

                        $timeout(function(){
                            $rootScope.uploading.status='err';

                        },TIME_MSG);
                        $scope.error = errorResponse.data.message;
                    });

                }
            });

        // This provides Authentication context.

        $scope.authentication = Authentication;
        $rootScope.headerType='paint';

        function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16)/255,
                g: parseInt(result[2], 16)/255,
                b: parseInt(result[3], 16)/255
            } : null;
        }
        var preload = new createjs.LoadQueue(true, 'modules/paint/img/brush/');
        var manifest = [
            {src: 'brush.png', id: 'brush'},
            {src: 'bunny.png', id: 'bunny'}
        ];
        preload.loadManifest(manifest);
        preload.addEventListener('complete', function (e) {
            var colorBrush = new createjs.Bitmap(preload.getResult('brush'));
            colorBrush.filters = [
                new createjs.ColorFilter(1,0.1,0.5,0.1)
            ];
            colorBrush.cache(0,0,preload.getResult('brush').width,preload.getResult('brush').height);

            $scope.brush.texture = new Image();
            $scope.brush.texture.src = colorBrush.getCacheDataURL();
            //console.log($scope.brush.texture);
        });

        $scope.brush = {
            scale: 1,
            color: {
                hex: null
            },
            jitter:40,
            interpol:{
                step:10,
                getDistance : function (point1, point2) {
                    var xs = 0;
                    var ys = 0;
                    xs = point2.x - point1.x;
                    xs = xs * xs;
                    ys = point2.y - point1.y;
                    ys = ys * ys;
                    return Math.sqrt(xs + ys);
                }
            }

        };

        $scope.$watch('brush.color.hex', function(newValue,oldValue){
            if(newValue !== oldValue){
                $scope.brush.color.rbg=hexToRgb($scope.brush.color.hex);
                var colorBrush = new createjs.Bitmap(preload.getResult('brush'));
                colorBrush.filters = [
                    new createjs.ColorFilter($scope.brush.color.rbg.r,$scope.brush.color.rbg.g,$scope.brush.color.rbg.b,0.1 )
                ];
                colorBrush.cache(0,0,preload.getResult('brush').width,preload.getResult('brush').height);
                $scope.brush.texture.src = colorBrush.getCacheDataURL();
            }
        });

        var stage = new createjs.Stage('paint-canvas');
        createjs.Ticker.addEventListener('tick', stage);
        createjs.Ticker.setFPS(60);
        createjs.Touch.enable(stage);

        var wrapper = new createjs.Container();
        wrapper.hitArea = new createjs.Shape(new createjs.Graphics().f('#000').dr(0, 0, 900, 500));
        wrapper.cache(0, 0, 900, 500); // Cache it.
        stage.addChild(wrapper);
        var drawing = new createjs.Shape();
        wrapper.addChild(drawing);
        var lastPoint = new createjs.Point();

        drawing.graphics.beginFill('#ff0000');
        drawing.graphics.drawCircle(10  , 10, 10) ;
        //stage.update();


        wrapper.addEventListener('mousedown', function (event) {
            lastPoint.x = event.stageX  ;
            lastPoint.y = event.stageY ;

            var last = new createjs.Point();
            last.x = event.stageX;
            last.y = event.stageY;

            Colaborative.realtime.emit('message',{position: last});


            event.addEventListener('mousemove', function (event) {
                var tVector = new createjs.Point();
                tVector.x = event.stageX;
                tVector.y = event.stageY;

                $scope.brush.interpol.distance = $scope.brush.interpol.getDistance(tVector, lastPoint);
                var dist =  $scope.brush.interpol.getDistance(tVector, last);

                if(dist > 100) {

                    last.x = event.stageX;
                    last.y = event.stageY;
                    Colaborative.realtime.emit('message',{position: last});
                }
                $scope.brush.interpol.step = $scope.brush.interpol.distance * (($scope.brush.texture.width * 0.0001 ) / $scope.brush.scale ) ;
                $scope.brush.interpol.angle = Math.atan2(event.stageX - lastPoint.x, event.stageY - lastPoint.y);

                var tmpCircleSize =$scope.brush.texture.width *0.5 * $scope.brush.scale;

                for (var r = 0; r < $scope.brush.interpol.step; r++) {
                    $scope.brush.interpol.x = lastPoint.x + ( Math.sin($scope.brush.interpol.angle)) * ($scope.brush.interpol.distance) * (r / $scope.brush.interpol.step);
                    $scope.brush.interpol.y = lastPoint.y + ( Math.cos($scope.brush.interpol.angle)) * ($scope.brush.interpol.distance) * (r / $scope.brush.interpol.step);
                    var matrix = new createjs.Matrix2D();
                    //matrix.transformPoint(0.5,0.5);
                    matrix.scale($scope.brush.scale,$scope.brush.scale);
                    var r1=Math.random() * $scope.brush.jitter;
                    var r2=Math.random() * $scope.brush.jitter;
                    r1= 0; r2=0;

                    //matrix.translate($scope.brush.interpol.x +r1 - ($scope.brush.texture.width * 0.25) , $scope.brush.interpol.y +r2 - ($scope.brush.texture.height * 0.25) );

                    matrix.translate($scope.brush.interpol.x - tmpCircleSize, $scope.brush.interpol.y  - tmpCircleSize);

                    drawing.graphics.beginBitmapFill($scope.brush.texture, 'repeat', matrix);
                    drawing.graphics.drawCircle($scope.brush.interpol.x + r1 , $scope.brush.interpol.y + r2, tmpCircleSize) ;

                }
                lastPoint.x = event.stageX;
                lastPoint.y = event.stageY;


                var erase = document.getElementById('toggle').checked;
                wrapper.updateCache(erase ? 'lighter' : 'source-over');
                drawing.graphics.clear();
            });
            //stage.update();
        });


    }
]);

