'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [

	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};
        _this._data.isSocialUser = function(social, user){
            var u= user || _this._data.user;
            if (u.provider === social){
                return true;
            }else{
                return false;
            }
        };
		return _this._data;
	}
]);