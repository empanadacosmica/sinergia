'use strict';

angular.module('users').controller('ProfileController', ['$rootScope','$scope', '$http', '$location', 'Users', 'Authentication','Oeuvres','$stateParams',
    function($rootScope, $scope, $http, $location, Users, Authentication, Oeuvres, $stateParams) {

        $scope.authentication = Authentication;

        if($scope.authentication.user){
            $rootScope.headerType='search';
        }else{
            $rootScope.headerType='brand';
        }

        $scope.load=function(){
            if($scope.authentication.user._id ===$stateParams.userId ){
                $scope.user=$scope.authentication.user;
                $scope.mine=true;
                Users.get().$promise.then(function(user){
                    $scope.user=user;
                }, function(err){
                    $location.path('/');
                });

            }else{
                Users.get({userId:$stateParams.userId}).$promise.then(function(user){
                    $scope.user=user;
                }, function(err){
                    $location.path('/');
                });
            }
        };

        $scope.order='-created';
        $scope.view='full';
        $scope.changeOrder=function(order){
            $scope.order=order;
        };
        $scope.changeViewStyle=function(view){
            $scope.view=view;
        };

        $scope.hasConnectedAdditionalSocialAccounts = function(provider) {
            for (var i in $scope.user.additionalProvidersData) {
                return true;
            }
            return false;
        };

        // Check if provider is already in use with current user
        $scope.isConnectedSocialAccount = function(provider) {
            return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
        };

        // Remove a user social account
        $scope.removeUserSocialAccount = function(provider) {
            $scope.success = $scope.error = null;

            $http.delete('/users/accounts', {
                params: {
                    provider: provider
                }
            }).success(function(response) {
                // If successful show success message and clear form
                $scope.success = true;
                $scope.user = Authentication.user = response;
            }).error(function(response) {
                $scope.error = response.message;
            });
        };

        // Update a user profile
        $scope.updateUserProfile = function() {

            $scope.success = $scope.error = null;
            var uuser = new Users.update($scope.user);

            uuser.$change(function(response) {
                $scope.success = true;
                Authentication.user = response;
            }, function(response) {
                $scope.error = response.data.message;
            });

        };

        // Change user password
        $scope.changeUserPassword = function() {
            $scope.success = $scope.error = null;

            $http.post('/users/password', $scope.passwordDetails).success(function(response) {
                // If successful show success message and clear form
                $scope.success = true;
                $scope.passwordDetails = null;
            }).error(function(response) {
                $scope.error = response.message;
            });
        };
    }
]);