'use strict';
angular.module('core').factory('common',[
    function() {
        return{
            humanDate : function(fecha){
                var f = new Date(fecha);
                return moment(f).format('L');
            },
            calcRating: function(oeuvre){
                if(oeuvre.rating.length <= 0){return 0;}
                var rating =0;
                var cRating = oeuvre.rating.length;

                for (var rIndex =0; rIndex < oeuvre.rating.length; rIndex ++){
                    rating = rating + oeuvre.rating[rIndex].calification;
                }
                return Math.round(rating / cRating);
            }
        };
    }]);