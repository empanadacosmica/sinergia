'use strict';
angular.module('core').directive('autoclose', function($rootScope, $timeout) {
        return{
            restrict: 'A',
            link: function(scope, ele, attrs) {
                $timeout(function(){
                    $rootScope.uploading.ocultar=true;
                    $timeout(function(){
                        $rootScope.uploading.status=null;
                    },1000);
                },2000);
            }
        };
    });

