'use strict';

angular.module('core').controller('NotificationController', ['$rootScope','$scope',
	function($rootScope, $scope, $timeout) {
        $scope.close= function(){
            $rootScope.uploading.ocultar=true;
            $timeout(function(){
                $rootScope.uploading.status=null;
            },1000);
        };
	}
]);