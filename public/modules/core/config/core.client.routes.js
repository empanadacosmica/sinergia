'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/');

        // Home state routing
        $stateProvider.
            state('home', {
                url: '/',
                templateUrl: function ($stateParams){
                    if(window.user){
                        return 'modules/atelier/views/atelier.client.view.html';

                    }else{
                        return 'modules/core/views/home.client.view.html';

                    }
                },
                controllerProvider: function ($stateParams){
                    if(window.user){
                        return 'AtelierController';
                    }else{
                        return 'HomeController';
                    }
                }

            });
    }
]);