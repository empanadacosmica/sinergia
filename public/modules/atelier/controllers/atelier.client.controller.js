'use strict';


angular.module('atelier').controller('AtelierController', ['$rootScope','$scope', 'Authentication','Oeuvres','common',
    function($rootScope, $scope, Authentication, Oeuvres, common) {

        // This provides Authentication context.
        $scope.authentication = Authentication;
        $scope.tool = common;
        var MAX_OEUVRES=10;
        var MAX_MYOEUVRES=4;
        $scope.limit=MAX_OEUVRES;


        $scope.rating = function(oeuvre, index){
            $scope.oeuvres[index].ratingprom= $scope.tool.calcRating(oeuvre);
        };

        $scope.load =function(){
             Oeuvres.oeuvresByUser({limit: MAX_MYOEUVRES}).$promise.then(function(myoeuvres){
                $scope.myOeuvres=myoeuvres;
             });

            $scope.loading=true;
            Oeuvres.query({limit: $scope.limit}).$promise.then(function (oeuvres) {
                $scope.loading=false;
                $scope.oeuvres=oeuvres;
                $scope.limit=$scope.limit + MAX_OEUVRES;
            }, function(){
                $scope.loading=false;
                $scope.noLoad=true;
            });

        };

        $rootScope.headerType='search';
        $scope.order='-created';
        $scope.view='mini';

        $scope.loading=true;


        $scope.changeOrder=function(order){
            $scope.order=order;
        };

        $scope.changeViewStyle=function(view){
            $scope.view=view;
        };
        $scope.find = function(forceLoad){
            if(!$scope.noLoad || forceLoad ){
                $scope.loading=true;

                Oeuvres.query({limit: $scope.limit}).$promise.then(function (oeuvres) {
                    $scope.loading=false;
                    for(var i=0; i < oeuvres.length; i++){
                        $scope.oeuvres.push(oeuvres[i]);
                    }
                    $scope.limit=$scope.limit + MAX_OEUVRES;
                }, function(){
                    $scope.loading=false;
                    $scope.noLoad=true;
                });
            }
        };

    }
]);
