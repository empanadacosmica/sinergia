'use strict';

// Setting up route
angular.module('oeuvre').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider) {
        // Redirect to home view when route not found

        // Home state routing
        $stateProvider.
            state('oeuvre', {
                url: '/oeuvre/:oeuvreId',
                templateUrl: 'modules/oeuvre/views/oeuvre.client.view.html',
                controller: 'oeuvreController'
            });
    }
]);