'use strict';

//Menu service used for managing  menus
angular.module('oeuvre').factory('Oeuvres', ['$resource',
    function($resource) {
        return $resource('oeuvres/:oeuvresId',{oeuvresId:'@oeuvresId'},
            {
                update: {
                    method: 'PUT'
                },
                oeuvresByUser:{
                    url:'oeuvres/user/:oeuvresId',
                    method:'GET',
                    isArray:true
                },
                comment: {
                    url:'oeuvres/comment/:oeuvresId',
                    method:'POST',
                    isArray:true
                }
            });
    }
]);
