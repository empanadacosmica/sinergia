'use strict';


angular.module('oeuvre').controller('oeuvreController', ['$rootScope', '$scope', 'Authentication','Oeuvres','$stateParams','$location','common',
    function($rootScope, $scope, Authentication, Oeuvres, $stateParams, $location, common) {
        $scope.authentication = Authentication;
        $scope.tool= common;


        if($scope.authentication.user){
            $rootScope.headerType='search';
        }else{
            $rootScope.headerType='brand';
        }

        $scope.comment = function(form){
            if(form.$valid) {
                var newComment = {
                    message: form.message,
                    user: $scope.authentication.user,
                    created: new Date()
                };
                var message=form.message;
                form.message=null;
                $scope.oeuvre.comments.push(newComment);

                newComment={
                    message: message
                };

                Oeuvres.comment({oeuvresId:$scope.oeuvre._id, comment:newComment}).$promise.then(function(comments){

                    $scope.oeuvre.comments=comments;

                }, function(err){
                    $scope.oeuvre.comments.splice($scope.oeuvre.comments.length-1, 1);
                });
            }
        };

        $scope.load=function(){
            Oeuvres.get({oeuvresId:$stateParams.oeuvreId}).$promise.then(function(oeuvre){
                $scope.oeuvre=oeuvre;
                $scope.oeuvre.ratingprom= $scope.tool.calcRating($scope.oeuvre);
                if($scope.authentication.user._id === $scope.oeuvre.user._id){
                    $scope.mine=true;
                }
            }, function(err){
                $location.path('/');
            });
        };

    }
]);

