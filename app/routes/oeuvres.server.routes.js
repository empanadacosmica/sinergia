'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users'),
	oeuvres = require('../../app/controllers/oeuvres');


module.exports = function(app) {
    oeuvres.cloudinary(app.cloudinary);
	// Article Routes
	app.route('/oeuvres')
		.get(oeuvres.list)
		.post(users.requiresLogin, oeuvres.create);

    app.route('/oeuvres/user')
        .get(oeuvres.listByUser);


    app.route('/oeuvres/:oeuvresId')
		.get(oeuvres.read)
		.put(users.requiresLogin, oeuvres.hasAuthorization, oeuvres.update)
		.delete(users.requiresLogin, oeuvres.hasAuthorization, oeuvres.delete);

    app.route('/oeuvres/comment/:oeuvresId')
        .post(users.requiresLogin, oeuvres.comment);

	// Finish by binding the article middleware
	app.param('oeuvresId', oeuvres.oeuvreByID);
};