'use strict';

/**
 * Module dependencies.
 */
var MAX_OEUVRES=10;
var mongoose = require('mongoose'),
    Oeuvres = mongoose.model('Oeuvres'),
    _ = require('lodash'),
    crypto = require('crypto'),
    Global = require('../../config/env/all'),
    fs = require('fs'),
    cloudinary = null;

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
    var message = '';

    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Article already exists';
                break;
            default:
                message = 'Something went wrong';
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message) message = err.errors[errName].message;
        }
    }

    return message;
};

/**
 * Create a article
 */

exports.cloudinary= function(appcluinary){
    cloudinary=appcluinary;
};

exports.create = function(req, res) {
    var oeuvre = new Oeuvres(req.body);
    oeuvre.user = req.user;

    var body = oeuvre.img,
        base64Data = body.replace(/^data:image\/png;base64,/,''),
        binaryData = new Buffer(base64Data, 'base64').toString('binary');
    oeuvre.img ='pending';
    oeuvre.save(function(err) {
        if (err) {
            return res.send(400, {
                message: getErrorMessage(err)
            });
        } else {
            var tmpPath =  oeuvre._id + '.png';
            fs.writeFile(Global.imagePath +  tmpPath , binaryData, 'binary', function(err) {
                if(!err){
                    if(process.env.NODE_ENV==='production') {
                        cloudinary.uploader.upload(Global.imagePath + tmpPath, function (result) {
                            oeuvre.img = result.url;
                            oeuvre.available = true;
                            oeuvre.save(function (err) {
                                if (err) {
                                    return res.jsonp(500, null);
                                }
                                return res.jsonp(oeuvre);
                            });
                        });
                    }else{
                        oeuvre.img = 'oeuvresfiles/' + oeuvre._id + '.png';
                        oeuvre.available = true;
                        oeuvre.save(function (err) {
                            if (err) {
                                return res.jsonp(500, null);
                            }
                            return res.jsonp(oeuvre);
                        });
                    }

                }else{
                    console.log(err);
                    return res.jsonp(oeuvre);
                }
            });

        }
    });



};

/**
 * Show the current article
 */
exports.read = function(req, res) {
    var oeuvre =req.oeuvre;
    var view = oeuvre.views + 1;
    oeuvre.views = view;
    oeuvre.save(function(err){
        if(err) req.send(500,{message:'error saving db'});
        res.jsonp(oeuvre);
    });

};

/**
 * Update a article
 */
exports.update = function(req, res) {
    var oeuvre = req.oeuvre;

    oeuvre = _.extend(oeuvre, req.body);

    oeuvre.save(function(err) {
        if (err) {
            return res.send(400, {
                message: getErrorMessage(err)
            });
        } else {
            res.jsonp(oeuvre);
        }
    });
};

exports.comment = function(req, res){
    var oeuvre = req.oeuvre;
    if(!req.body.comment){
        res.jsonp(500,{message: 'no message exist'});
    }
    var comment = req.body.comment;
    comment.user=req.user;

    oeuvre.comments.push(comment);

    oeuvre.save(function(err){
        if(err){res.jsonp(501, {message:'err writing de bbdd'});}
        res.jsonp(oeuvre.comments);
    });

};


/**
 * Delete an article
 */
exports.delete = function(req, res) {
    var oeuvre = req.oeuvre;

    oeuvre.remove(function(err) {
        if (err) {
            return res.send(400, {
                message: getErrorMessage(err)
            });
        } else {
            res.jsonp(oeuvre);
        }
    });
};

/**
 * List of Articles
 */
exports.list = function(req, res) {
    var limit = req.query.limit || null;
    var skip =  null;
    if(limit>=10){
        skip =limit-MAX_OEUVRES;
    }
    Oeuvres.find({available: true}).sort('-created').populate('user', 'displayName _id provider providerData').skip(skip).limit(limit).exec(function(err, oeuvres) {
        if (err) {
            return res.send(400, {
                message: getErrorMessage(err)
            });
        } else {
            if(oeuvres.length <= 0)
            {
                res.send(404, {
                    message: 'No more ouevures founds'
                });
            }else{
                res.jsonp(oeuvres);

            }
        }
    });
};

exports.listByUser = function(req,res){
    var query = req.user;
    if(req.query.user){
        query=req.query.user;
    }
    if(!req.query.limit){
        Oeuvres.find({user: query, available: true}).sort('-created').populate('user', 'displayName _id provider providerData').exec(function(err, oeuvres) {
            if (err) res.send(500, {message:err});
            if (!oeuvres) res.send(400, {oevures:null});
            res.jsonp(oeuvres);
        });
    }else{
        Oeuvres.find({user: query, available: true }).sort('-created').populate('user', 'displayName _id provider providerData').limit(req.query.limit).exec(function(err, oeuvres) {
            if (err) res.send(500, {message:err});
            if (!oeuvres) res.send(400, {oevures:null});
            res.jsonp(oeuvres);
        });
    }
};

/**
 * Article middleware
 */
exports.oeuvreByID = function(req, res, next, id) {
    Oeuvres.findById(id).populate('user', 'displayName _id provider providerData').populate('comments.user').exec(function(err, oeuvre) {
        if (err) return next(err);
        if (!oeuvre) return next(new Error('Failed to load oeuvre ' + id));
        req.oeuvre = oeuvre;
        next();
    });
};

/**
 * Article authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.oeuvre.user.id !== req.user.id) {
        return res.send(403, {
            message: 'User is not authorized'
        });
    }
    next();
};