'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Oeuvres Schema
 */
var OeuvresSchema = new Schema({
    available : {
        type: Boolean,
        default:false
    },
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: '',
        required: 'Title cannot be blank'
    },
    description:{
        type:String,
        default:'',
        trim: false
    },
    views:{
        type: Number,
        default : 0
    },
    rating:[{
        user:{
            type: Schema.ObjectId,
            ref: 'User'
        },
        calification:{
            type: Number,
            default:0
        }
    }],
    comments:[
        {
            created: {
                type: Date,
                default: Date.now
            },
            message: {
                type: String,
                required: 'Message cannot be blank'
            },
            user: {
                type: Schema.ObjectId,
                ref: 'User'
            }
        }
    ],
    img: {
        type: String,
        default: ''
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Oeuvres', OeuvresSchema);