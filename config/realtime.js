'use strict';

var Server = require('socket.io'),
    cookieParser = require('cookie-parser'),
    config = require('./config'),
    passportSocketIo = require('passport.socketio');

module.exports = function(io, app ,server) {

    function onAuthorizeSuccess(data, accept){
        console.log('successful connection to socket.io');
        accept();
    }

    function onAuthorizeFail(data, message, error, accept){
        if(error)
            throw new Error(message);
        console.log('failed connection to socket.io:', message);
        if(error)
            accept(new Error(message));
    }


    io = new Server(server);


    io.use(passportSocketIo.authorize({
        cookieParser: cookieParser,
        key:         'connect.sid',
        secret:      config.sessionSecret,
        store:       app.session,
        success:     onAuthorizeSuccess,
        fail:        onAuthorizeFail,
    }));

    io.on('connection', function (socket) {
        var userId = socket.request.user;
        //console.log(userId);
        socket.on('message', function (from) {
            socket.broadcast.emit('mymessage', from);
        });
    });

};