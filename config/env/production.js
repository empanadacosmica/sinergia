'use strict';

module.exports = {
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/mean',
	/*assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
			],
			js: [
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	},*/
	facebook: {
		clientID: process.env.FACEBOOK_ID || '1454360828155813',
		clientSecret: process.env.FACEBOOK_SECRET || '6ca87da0c02a56eed174d5e1a62a2420',
		callbackURL: 'http://sinergi.herokuapp.com/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'GdabYimbupgROSVWlqQEIxr3l',
		clientSecret: process.env.TWITTER_SECRET || 'aUmNGDb92IJH6wPudiFrrdXAkjYpNhVXboBhJ0GH0esIObT3ts',
		callbackURL: 'http://sinergi.herokuapp.com/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '873348076288-117tciff5343414ud5aia83qdokm28i5.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || '7F4MjUVEA3dXZemk4Fp2BdM8',
		callbackURL: 'http://sinergi.herokuapp.com/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	}
};