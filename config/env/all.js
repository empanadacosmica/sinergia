'use strict';

module.exports = {
	app: {
		title: 'Sinergia: creaciones colaborativas',
		description: 'Plataforma de creaciones colaborativas',
		keywords: 'colaborative, paiting, puntira, colaborativo, dibujo, creaciones sociales, social creation, drawing'
	},
    cloudinary:{
        name: 'doacqvmgu',
        API_KEY: '821362779191818',
        API_SECRET: 'M-95gGkhhFHAn8ebpc7Yc7Hs3hE',
        URL: 'CLOUDINARY_URL=cloudinary://821362779191818:M-95gGkhhFHAn8ebpc7Yc7Hs3hE@doacqvmgu'
    },
    imagePath: 'public/oeuvresfiles/',
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
    sessionSecret: 'Siner',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
                'public/lib/font-awesome/css/font-awesome.min.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/easeljs/lib/easeljs-0.7.1.min.js',
                'public/lib/PreloadJS/lib/preloadjs-0.4.1.min.js',
                'public/lib/ngInfiniteScroll/build/ng-infinite-scroll.min.js',
                'public/lib/socket.io-client/socket.io.js',
                'public/lib/angular-socket-io/socket.min.js',
                'public/lib/moment/min/moment-with-locales.min.js'
            ]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};