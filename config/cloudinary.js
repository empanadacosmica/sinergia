var cloudinary = require('cloudinary'),
    config = require('./config');

cloudinary.config({
    cloud_name: config.cloudinary.name,
    api_key: config.cloudinary.API_KEY,
    api_secret: config.cloudinary.API_SECRET
});

module.exports = cloudinary;